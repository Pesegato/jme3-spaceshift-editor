# jMonkeyEngine 3 SpaceShift Editor 0.6.0 #

* Download: https://www.dropbox.com/sh/o7202c22pfyrhpv/AAC8eN57tkydThLRMxmnYXP9a?dl=0

## [Video about this editor.](https://www.youtube.com/watch?v=hgmujIUlyH8&feature=youtu.be) ##

## ver. 0.6.0 ##
* -Implemented undo-redo(ctrl+z/ctrl+y) in Material Editor and Model Editor.

## ver. 0.5.2 ##
* -Implemented the autocomplete for type of material in material creator.
* -Fixed the problems with focus in dialogs of editor.
* -Added the animation of loading for editors.

## ver. 0.5.0 ##
* -Fixed the exception with LwjglWindow on Windows;
* -Implemented the camera like camera of Blender.
* -Fixed the text input.
* -Implemented the hotkey 'delete' to delete files in tree.
* -Implemented the manipulators in Model Editor.
* -Updated the action for generating tangents.
* -Changed rotation property of model to Euler in Model Editor.

## ver. 0.4.5 ##
* -Added the text editor for GLSL with highlighting.
* -Added the reloading materials when change shaders.
* -Updated the logic for change of material type.
* -Fixed problems with menu bar.
* -Added the action for closing editor to menu bar.
* -Added tooltip with fullpath for root folder in asset tree.
* -Added the action for reopening last asset folders.
* -Implemented the saving of opened files.
* -Updated the jME to alpha 3 with PBR.
* -Migrated the editor from LWJGL 2.9 to LWJGL 3.0
* -Added the combobox with Queue Bucket for model of material preview to material editor.
* -Implemented the save on ctrl+S hotkey.

## ver. 0.4.0 ##
* -Added the action for creating a folder.
* -Added the action for creating an empty file.
* -Implemented image viewer.
* -Added actions for creating node, box, sphere and quad in ModelEditor.
* -Added action for creating SkyBox.
* -Added action for loading other model to model in Model Editor.
* -Fixed severals bugs.

## ver. 0.3.7 ##
* -Implemented the fullscreen mode.
* -Implemented the preview of channels of image.
* -Implemented the cache of preview of image.
* -Changed the sensitivity of zoom.

## ver. 0.3.5 ##
* -Implemeted open file in external editor.
* -Finished implementing the operations of moving/renaming/cut-copy-paste for files in Asset Tree.
* -Added dialog for handling exception.

## ver. 0.3.0 ##
* -Implemented Simple Model Editor.
* -Added action for converting .blend to .j3o.
* -Updated UI.

## ver. 0.2.0 ##
* -Implemented dialogs for creating new material and post filter view file.
* -Implemented dialog of graphic settings.
* -Updated the sky in material editor.
* -Upgraded jME3 libraries to 3.1 alpha 2 with PBR.

## ver. 0.1.1 ##
* -Implemented preview of images in resource selector.
* -Migrated editor to jME 3.1 with PBR.
* -Added support PBR to Material Editor.
* -Finished implementation PostFilterViewer.
## ver. 0.1 ##
* -Implemented the base actions in AssetTree.
* -Implemented MaterialEditor.
* -Added English localization.