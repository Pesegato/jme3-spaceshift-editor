package com.ss.editor.state.editor;

import com.jme3.app.state.AppState;

/**
 * Интерфейс для реализации 3D части редактора.
 *
 * @author Ronn
 */
public interface EditorState extends AppState {
}
