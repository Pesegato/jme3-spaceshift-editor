package com.ss.editor;

/**
 * Перечень всех интересуемых расширений.
 *
 * @author Ronn
 */
public interface FileExtensions {

    public static final String JME_MATERIAL = "j3m";
    public static final String JME_MATERIAL_DEFINITION = "j3md";
    public static final String JME_OBJECT = "j3o";

    public static final String POST_FILTER_VIEW = "pfv";

    public static final String IMAGE_PNG = "png";
    public static final String IMAGE_JPG = "jpg";
    public static final String IMAGE_JPEG = "jpeg";
    public static final String IMAGE_GIF = "gif";
    public static final String IMAGE_TGA = "tga";
    public static final String IMAGE_BMP = "bmp";
    public static final String IMAGE_TIFF = "tiff";
    public static final String IMAGE_DDS = "dds";
    public static final String IMAGE_HDR = "hdr";

    public static final String GLSL_VERTEX = "vert";
    public static final String GLSL_FRAGMENT = "frag";

    public static final String BLENDER = "blend";
}