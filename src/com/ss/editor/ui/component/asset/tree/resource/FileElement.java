package com.ss.editor.ui.component.asset.tree.resource;

import java.nio.file.Path;

/**
 * Реализация элемента для файла.
 *
 * @author Ronn
 */
public class FileElement extends ResourceElement {

    public FileElement(final Path file) {
        super(file);
    }
}
