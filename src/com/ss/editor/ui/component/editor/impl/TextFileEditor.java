package com.ss.editor.ui.component.editor.impl;

import com.ss.editor.Messages;
import com.ss.editor.ui.component.editor.EditorDescription;
import com.ss.editor.ui.component.editor.EditorRegistry;
import com.ss.editor.ui.css.CSSClasses;
import com.ss.editor.ui.css.CSSIds;

import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;

import javafx.scene.control.TextArea;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import rlib.ui.util.FXUtils;
import rlib.util.FileUtils;
import rlib.util.StringUtils;

/**
 * Реализация редактора текстовых файлов.
 *
 * @author Ronn
 */
public class TextFileEditor extends AbstractFileEditor<VBox> {

    public static final EditorDescription DESCRIPTION = new EditorDescription();

    static {
        DESCRIPTION.setConstructor(TextFileEditor::new);
        DESCRIPTION.setEditorName(Messages.TEXT_FILE_EDITOR_NAME);
        DESCRIPTION.setEditorId(TextFileEditor.class.getName());
        DESCRIPTION.addExtension(EditorRegistry.ALL_FORMATS);
    }

    /**
     * Контент на момент открытия документа.
     */
    private String originalContent;

    /**
     * Область для редактирования текста.
     */
    private TextArea textArea;

    @Override
    protected VBox createRoot() {
        return new VBox();
    }

    @Override
    protected void createContent(final VBox root) {

        textArea = new TextArea();
        textArea.setId(CSSIds.TEXT_EDITOR_TEXT_AREA);
        textArea.textProperty().addListener((observable, oldValue, newValue) -> updateDirty(newValue));

        FXUtils.addToPane(textArea, root);
        FXUtils.addClassTo(textArea, CSSClasses.MAIN_FONT_13);
        FXUtils.bindFixedSize(textArea, root.widthProperty(), root.heightProperty());
    }

    /**
     * Обновление состояния измененности.
     */
    private void updateDirty(final String newContent) {
        setDirty(!getOriginalContent().equals(newContent));
    }

    @Override
    protected boolean needToolbar() {
        return true;
    }

    @Override
    protected void createToolbar(final HBox container) {
        super.createToolbar(container);
        FXUtils.addToPane(createSaveAction(), container);
    }

    /**
     * @return область для редактирования текста.
     */
    private TextArea getTextArea() {
        return textArea;
    }

    @Override
    public void openFile(final Path file) {
        super.openFile(file);

        final byte[] content = FileUtils.getContent(file);

        if (content == null) {
            setOriginalContent(StringUtils.EMPTY);
        } else {
            setOriginalContent(new String(content));
        }

        final TextArea textArea = getTextArea();
        textArea.setText(getOriginalContent());
    }

    /**
     * @return контент на момент открытия документа.
     */
    public String getOriginalContent() {
        return originalContent;
    }

    /**
     * @param originalContent контент на момент открытия документа.
     */
    public void setOriginalContent(final String originalContent) {
        this.originalContent = originalContent;
    }

    @Override
    public void doSave() {
        super.doSave();

        final TextArea textArea = getTextArea();
        final String newContent = textArea.getText();

        try (final PrintWriter out = new PrintWriter(Files.newOutputStream(getEditFile()))) {
            out.print(newContent);
        } catch (final IOException e) {
            LOGGER.warning(this, e);
        }

        setOriginalContent(newContent);
        updateDirty(newContent);
        notifyFileChanged();
    }

    @Override
    public EditorDescription getDescription() {
        return DESCRIPTION;
    }
}
