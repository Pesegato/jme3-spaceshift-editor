package com.ss.editor.ui.css;

/**
 * Интерфейс с набором констант ID используемых в CSS.
 *
 * @author Ronn
 */
public interface CSSIds {

    public static final String EDITOR_BAR_COMPONENT = "EditorBarComponent";
    public static final String EDITOR_LOADING_LAYER = "EditorLoadingLayer";
    public static final String EDITOR_LOADING_PROGRESS = "EditorLoadingProgress";

    public static final String MAIN_SPLIT_PANEL = "MainSplitPane";

    public static final String ASSET_COMPONENT = "AssetComponent";
    public static final String ASSET_COMPONENT_BAR = "AssetComponentBar";
    public static final String ASSET_COMPONENT_BAR_BUTTON = "AssetComponentBarButton";
    public static final String ASSET_COMPONENT_RESOURCE_TREE_CELL = "AssetComponentResourceTreeCell";

    public static final String EDITOR_AREA_COMPONENT = "EditorAreaComponent";

    public static final String FILE_EDITOR_TOOLBAR = "FileEditorToolbar";

    public static final String TEXT_EDITOR_TEXT_AREA = "TextEditorTextArea";

    public static final String POST_FILTER_EDITOR_MATERIAL_FILTER_CONTAINER = "PostFilterEditorMaterialListContainer";
    public static final String POST_FILTER_EDITOR_ADD_MATERIAL_BUTTON = "PostFilterEditorAddMaterialButton";
    public static final String POST_FILTER_EDITOR_CELL_REMOVE_BUTTON = "PostFilterEditorCellRemoveButton";

    public static final String EDITOR_DIALOG_BACKGROUND = "EditorDialogBackground";
    public static final String EDITOR_DIALOG_HEADER = "EditorDialogHeader";
    public static final String EDITOR_DIALOG_HEADER_BUTTON_CLOSE = "EditorDialogHeaderButtonClose";
    public static final String EDITOR_DIALOG_BUTTON_OK = "EditorDialogButtonOk";
    public static final String EDITOR_DIALOG_BUTTON_CANCEL = "EditorDialogButtonCancel";

    public static final String ASSET_EDITOR_DIALOG_BUTTON_CONTAINER = "AssetEditorDialogButtonContainer";
    public static final String ASSET_EDITOR_DIALOG_PREVIEW_CONTAINER = "AssetEditorDialogPreviewContainer";

    public static final String MATERIAL_FILE_EDITOR_TOOLBAR_LABEL = "MaterialFileEditorToolbarLabel";
    public static final String MATERIAL_FILE_EDITOR_TOOLBAR_BOX = "MaterialFileEditorToolbarBox";
    public static final String MATERIAL_FILE_EDITOR_TOOLBAR_SMALL_BOX = "MaterialFileEditorToolbarSmallBox";
    public static final String MATERIAL_FILE_EDITOR_PARAMETER_CONTAINER = "MaterialFileEditorParameterContainer";

    public static final String MATERIAL_PARAM_CONTROL_PARAM_NAME = "MaterialParamControlParamName";
    public static final String MATERIAL_PARAM_CONTROL_BUTTON = "MaterialParamControlButton";
    public static final String MATERIAL_PARAM_CONTROL_CHECKBOX = "MaterialParamControlCheckbox";
    public static final String MATERIAL_PARAM_CONTROL_SPINNER = "MaterialParamControlSpinner";

    public static final String TEXTURE_2D_MATERIAL_PARAM_CONTROL_PREVIEW = "Texture2DMaterialParamControlPreview";

    public static final String MATERIAL_RENDER_STATE_POLY_OFFSET_FIELD = "MaterialRenderStatePolyOffsetField";

    public static final String FILE_CREATOR_LABEL = "FileCreatorLabel";
    public static final String FILE_CREATOR_TEXT_FIELD = "FileCreatorTextField";

    public static final String GRAPHICS_DIALOG_MESSAGE_LABEL = "GraphicsDialogMessageLabel";
    public static final String GRAPHICS_DIALOG_LABEL = "GraphicsDialogLabel";
    public static final String GRAPHICS_DIALOG_FIELD = "GraphicsDialogField";

    public static final String MODEL_FILE_EDITOR_PARAMETER_CONTAINER = "ModelFileEditorParameterContainer";

    public static final String MODEL_NODE_TREE_CELL = "ModelNodeTreeCell";
    public static final String MODEL_NODE_TREE_CELL_DRAGGED = "ModelNodeTreeCellDragged";
    public static final String MODEL_NODE_TREE_CELL_DROP_AVAILABLE = "ModelNodeTreeCellDropAvailable";

    public static final String MODEL_PARAM_CONTROL_SPLIT_LINE = "ModelParamControlSplitLine";
    public static final String MODEL_PARAM_CONTROL_PARAM_NAME = "ModelParamControlParamName";
    public static final String MODEL_PARAM_CONTROL_PARAM_NAME_SINGLE_ROW = "ModelParamControlParamNameSingleRow";
    public static final String MODEL_PARAM_CONTROL_NUMBER_LABEL = "ModelParamControlNumberLabel";
    public static final String MODEL_PARAM_CONTROL_VECTOR3F_FIELD = "ModelParamControlVector3fField";
    public static final String MODEL_PARAM_CONTROL_VECTOR4F_FIELD = "ModelParamControlVector4fField";
    public static final String MODEL_PARAM_CONTROL_COMBO_BOX = "ModelParamControlComboBox";

    public static final String MODEL_PARAM_CONTROL_MATERIAL_LABEL = "ModelParamControlMaterialLabel";
    public static final String MODEL_PARAM_CONTROL_MATERIAL_BUTTON = "ModelParamControlMaterialButton";

    public static final String RENAME_DIALOG_LABEL = "RenameDialogLabel";
    public static final String RENAME_DIALOG_TEXT_FIELD = "RenameDialogTextField";

    public static final String IMAGE_CHANNEL_PREVIEW = "ImageChannelPreview";
    public static final String IMAGE_CHANNEL_PREVIEW_IMAGE_CONTAINER = "ImageChannelPreviewImageContainer";

    public static final String CHOOSE_TEXTURE_CONTROL_TEXTURE_LABEL = "ChooseTextureControlTextureLabel";
    public static final String CHOOSE_TEXTURE_CONTROL_PREVIEW = "ChooseTextureControlPreview";

    public static final String CREATE_SKY_DIALOG_LABEL = "CreateSkyDialogLabel";
    public static final String CREATE_SKY_DIALOG_COMBO_BOX = "CreateSkyDialogComboBox";
    public static final String CREATE_SKY_DIALOG_SPINNER = "CreateSkyDialogSpinner";
    public static final String CREATE_SKY_DIALOG_BUTTON = "CreateSkyDialogButton";

    public static final String GENERATE_TANGENTS_DIALOG_LABEL = "GenerateTangentsDialogLabel";
    public static final String GENERATE_TANGENTS_DIALOG_COMBO_BOX = "GenerateTangentsDialogComboBox";
}
